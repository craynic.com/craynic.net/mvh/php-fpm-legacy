FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3003
RUN apt-get update \
    && apt-get install -y --no-install-recommends zip=3.* unzip=6.* software-properties-common=0.* \
    && add-apt-repository -u ppa:ondrej/php \
    && apt-get install -y --no-install-recommends \
        msmtp-mta=1.* \
        inotify-tools=3.* \
        supervisor=4.* \
        cron=3.* \
        php5.6-fpm=* \
        php5.6=* \
        php5.6-bcmath=* \
        php5.6-bz2=* \
        php5.6-cli=* \
        php5.6-common=* \
        php5.6-curl=* \
        php5.6-gd=* \
        php5.6-gmp=* \
        php5.6-imap=* \
        php5.6-imagick=3.* \
        php5.6-intl=* \
        php5.6-json=* \
        php5.6-mbstring=* \
        php5.6-mcrypt=* \
        php5.6-memcache=* \
        php5.6-mysql=* \
        php5.6-opcache=* \
        php5.6-pspell=* \
        php5.6-readline=* \
        php5.6-soap=* \
        php5.6-sqlite3=* \
        php5.6-tidy=* \
        php5.6-xml=* \
        php5.6-xmlrpc=* \
        php5.6-xsl=* \
        php5.6-yaml=* \
        php5.6-zip=* \
        gsfonts=1:* \
        libcairo2=* \
        libpango-1.0-0=* \
        libpangocairo-1.0-0=* \
        libhpdf-2.3.0=* \
        libpng16-16=* \
        php5.6-dev=* \
        make=* \
        git=* \
        php-pear=* \
        libcairo2-dev=* \
        libhpdf-dev=* \
        libpango1.0-dev=* \
        libpng-dev=* \
    && pecl install cairo-0.3.2 \
    && pecl install haru-1.0.4 \
    && git clone https://github.com/mgdm/php-pango /tmp/php-pango/ \
    && (cd /tmp/php-pango/ && phpize && ./configure && make && make install) \
    && rm -rf /tmp/php-pango/ \
    && rm -f /etc/cron.d/* /etc/cron.daily/* /etc/cron.hourly/* /etc/cron.monthly/* /etc/cron.weekly/* \
    && apt-get install -y --no-install-recommends \
        logrotate=3.* \
    && apt-get remove -y \
        zip unzip software-properties-common php5.6-dev make git php-pear libcairo2-dev libhpdf-dev libpango1.0-dev \
        libpng-dev \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV LOG_LEVEL="warn" \
    MAIL_RELAYHOST="" \
    MAIL_TLS_KEYFILE="" \
    MAIL_TLS_CRTFILE="" \
    MAIL_TLS_CAFILE="" \
    PHP_POOL_D="" \
    PHP_CONTAINER_DEBUGGING=false

COPY files/ /

VOLUME /etc/php/5.6/fpm/pool.d
VOLUME /var/www

WORKDIR /var/www

RUN chmod go-w /etc/logrotate.d/craynic \
    && sed -i "s/^\(error_log\s*=.*$\)/;\1\nerror_log = \/dev\/stderr/" /etc/php/5.6/fpm/php-fpm.conf \
    && phpenmod -v 5.6 -s fpm cairo craynic haru pango \
    && update-alternatives --install /usr/sbin/php-fpm php-fpm /usr/sbin/php-fpm5.6 1 \
    && mkdir -p /run/php/

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/healthcheck.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]

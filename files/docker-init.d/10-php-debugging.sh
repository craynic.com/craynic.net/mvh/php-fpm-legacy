#!/usr/bin/env bash

set -Eeuo pipefail

PHP_CONTAINER_DEBUGGING=${PHP_CONTAINER_DEBUGGING:-false}

if [[ "$PHP_CONTAINER_DEBUGGING" != true ]]; then
  exit 0
fi

echo "PHP container debugging requested, modifying php.ini..."

PHP_INI_FILE="/etc/php/5.6/mods-available/craynic.ini"

# enable expose_php
sed -i "s/^expose_php\s*=.*$/expose_php = On/g" -- "$PHP_INI_FILE"
# enable phpinfo
sed -i "s/^disable_functions\s*=\(.*\)\(phpinfo,\?\)\(.*\)$/disable_functions = \1\3/g" -- "$PHP_INI_FILE"

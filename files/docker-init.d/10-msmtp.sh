#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$MAIL_RELAYHOST" ]]; then
  echo "Mail configuration not provided, running without mail support."
  exit 0
fi

# ensure that either none or both keyfile and crtfile are defined
if [[ -n "$MAIL_TLS_KEYFILE" || -n "$MAIL_TLS_CRTFILE" ]]; then
  if [[ -z "$MAIL_TLS_KEYFILE" || -z "$MAIL_TLS_CRTFILE" ]]; then
    echo "Please define both MAIL_TLS_KEYFILE and MAIL_TLS_CRTFILE." >&2
    exit 1
  fi
fi

MSMTPRC_PATH="/etc/msmtprc"

# update the MSMTP configuration file with the mandatory ENV variables
cat <<EOF >"$MSMTPRC_PATH"
account default
host $MAIL_RELAYHOST
tls on
tls_starttls on
EOF

# if client certificate & key are provided, enable TLS client authentication
if [[ -n "$MAIL_TLS_KEYFILE" || -n "$MAIL_TLS_CRTFILE" ]]; then
  cat <<EOF >>"$MSMTPRC_PATH"
tls_key_file $MAIL_TLS_KEYFILE
tls_cert_file $MAIL_TLS_CRTFILE
EOF
fi

# if CA certificate is provided, enable server certificate check
if [[ -n "$MAIL_TLS_CAFILE" ]]; then
  cat <<EOF >>"$MSMTPRC_PATH"
tls_trust_file $MAIL_TLS_CAFILE
tls_certcheck on
EOF
else
  cat <<EOF >>"$MSMTPRC_PATH"
tls_certcheck off
EOF
fi

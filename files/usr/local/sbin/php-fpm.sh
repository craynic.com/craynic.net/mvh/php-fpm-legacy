#!/usr/bin/env bash

set -Eeuo pipefail

if [[ "$PHP_POOL_D" != "" ]]; then
  sed -i "s/^\(include\s*=.*$\)//" /etc/php/*/fpm/php-fpm.conf
  echo "include = ${PHP_POOL_D%/}/*.conf" >> /etc/php/*/fpm/php-fpm.conf
fi

trap 'kill -USR1 "${CHILD_PID}"; wait "${CHILD_PID}"' SIGUSR1
trap 'kill -USR2 "${CHILD_PID}"; wait "${CHILD_PID}"' SIGUSR2
trap 'kill -QUIT "${CHILD_PID}"; wait "${CHILD_PID}"; exit 0' SIGQUIT
trap 'kill -INT "${CHILD_PID}"; wait "${CHILD_PID}"; exit 0' SIGINT
trap 'kill -TERM "${CHILD_PID}"; wait "${CHILD_PID}"; exit 0' SIGTERM

/usr/sbin/php-fpm --nodaemonize &
CHILD_PID="$!"
wait "${CHILD_PID}"

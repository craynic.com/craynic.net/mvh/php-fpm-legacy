#!/usr/bin/env bash

set -Eeuo pipefail

# if php-fpm is installed
if [[ -x $(which php-fpm) ]]; then
  # if the pool.d directory is ready
  if /usr/local/sbin/pool-directory-status.sh 2>/dev/null; then
    # but the php-fpm is not running
    if [[ -x $(which supervisorctl) ]] && ! supervisorctl status php-fpm >/dev/null; then
      echo "Pool directory is ready and not empty, but PHP-FPM is not running" >&2
      exit 1
    fi
  fi
fi

# explicit exit code
exit 0
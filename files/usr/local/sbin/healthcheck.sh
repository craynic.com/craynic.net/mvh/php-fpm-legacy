#!/usr/bin/env bash

set -Eeuo pipefail

export HEALTHCHECK_PORTS="$*"

# run all healthcheck scripts
run-parts --exit-on-error --regex='\.sh$' -- "/usr/local/sbin/healthcheck.d"

# explicit exit code
exit 0

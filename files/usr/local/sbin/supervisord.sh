#!/usr/bin/env bash

set -Eeuo pipefail

# run supervisord
/usr/bin/supervisord -c /etc/supervisor/supervisord.conf -n
